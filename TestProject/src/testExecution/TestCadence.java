package testExecution;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import methodImplementaion.FirstPageMethod;
import methodImplementaion.SecondPageMethod;
import methodImplementaion.ThirdPageMethod;

public class TestCadence {
	WebDriver driver;
	FirstPageMethod firstPageMethodObj;
	SecondPageMethod secondPageMethodObj;
	ThirdPageMethod thirdPageMethodObj;
	
	
	@BeforeTest
     public void setup(){
		driver=new FirefoxDriver();
		driver.get("http://sydneytesters.herokuapp.com");
		
	}
	
	
	@Test(priority=0)
	public void homeScreenValidate() throws Exception{
		
		firstPageMethodObj=new FirstPageMethod(driver);
		
		
		//Validating the 1st Screen
		String mainPageTitle=firstPageMethodObj.getSydTestInsuranceTitle();
		
		Assert.assertTrue(mainPageTitle.contains("Sydney Testers Insurance"));
		
		//Click on the Car Quote Button 
		
		firstPageMethodObj.clickCarQtBtn();
		
		//Validate the Second Screen
		
		String secondPageTitle=firstPageMethodObj.getSydTestCarInsuranceTitle();
		
		Assert.assertTrue(secondPageTitle.contains("Sydney Testers Car Insurance"));
		

		secondPageMethodObj=new SecondPageMethod(driver);
		
		// Selecting the card Model
		
		secondPageMethodObj.selCarBrand();
		
		// Selecting the Car Make year
		
		secondPageMethodObj.typCarYear();
	
		// Selecting the Driver Age
		
		secondPageMethodObj.typDrvAge();
		
		// gender,state, email,clickquoteBtn
		
		secondPageMethodObj.selGender();
		
		// Select the state
		
		secondPageMethodObj.selState();
		
		// Type email
		
		secondPageMethodObj.typeMailID();
		
		// CLick on the quoteBtn
		
		Thread.sleep(5000);
		
		secondPageMethodObj.clkCarQtBtn();
		
	thirdPageMethodObj = new ThirdPageMethod(driver);
	
	//Validate the Sydney Testers Life Insurance
	
	       String ThirdScreen = thirdPageMethodObj.getSydTestLifeInsuranceTitle();
	       Assert.assertTrue(ThirdScreen.toLowerCase().contains("sydney testers life insurance"));
	
	String premium = thirdPageMethodObj.totalPreAmt();
	Assert.assertTrue(premium.contains("$57.52"));
	}
	
	@AfterTest
	public void teardown(){
	driver.close();
	}
	}
	
	
	

