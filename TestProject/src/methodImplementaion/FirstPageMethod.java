package methodImplementaion;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import itemID.FirstPageItemID;

public class FirstPageMethod implements FirstPageItemID {

	@FindBy(xpath=FirstPageItemID.sydTestersInsuranceTitleID)
	public WebElement sydTestInsuranceTitle;

    @FindBy (id=FirstPageItemID.carQuoteBtnID)
    public WebElement carQtBtn;
    
	@FindBy(xpath=FirstPageItemID.sydTestersInsuranceTitleID)
	public WebElement sydTestCarInsuranceTitle;
    
	
	
    
  WebDriver driver;
  
  public FirstPageMethod(WebDriver driver){
	  this.driver = driver;
	  
	//This initElements method will create all WebElements
	  PageFactory.initElements(driver, this);
  }
  
  

  //Verify the HomePage Title

  public String getSydTestInsuranceTitle(){
	  return sydTestInsuranceTitle.getText();
  }
  

  
  //Click on the CarQuoteBtn
  
  public void clickCarQtBtn(){
	  carQtBtn.click();
  }
  

 //Verify that the screen Navigated to Next Screen
  
  public String getSydTestCarInsuranceTitle(){
	  return sydTestCarInsuranceTitle.getText();
  }
  
  
}
    