package methodImplementaion;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import itemID.ThirdPageItemID;

public class ThirdPageMethod implements ThirdPageItemID {

	@FindBy(xpath=ThirdPageItemID.sydTestersLifeInsuranceID)
	public WebElement sysTestLifeInsuranceTitle;
	
	@FindBy(xpath=ThirdPageItemID.PremiumTotalAmountID)
	public WebElement totalPremiumAount;
	
	
	WebDriver driver;
	
	public ThirdPageMethod(WebDriver driver){
	this.driver=driver;
	 PageFactory.initElements(driver, this);
	}
	
	//Verify the Life Insurance Title
	
	public String getSydTestLifeInsuranceTitle(){
		return sysTestLifeInsuranceTitle.getText();
		
	}

	public String totalPreAmt(){
			return totalPremiumAount.getText();	
	}
	
		
		
		
		
}
