package methodImplementaion;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import itemID.SecondPageItemID;

public class SecondPageMethod implements SecondPageItemID{

    @FindBy (id=SecondPageItemID.makeID )
     WebElement carBrand;
    
    @FindBy (id=SecondPageItemID.yearID)
    public WebElement carYear;
    
    @FindBy (id=SecondPageItemID.driverAgeID)
    public WebElement driverAge;
    
    @FindBy (id=SecondPageItemID.driverGenderID)
    public WebElement personGender;
    
    @FindBy (id=SecondPageItemID.stateID)
    public WebElement state;
    
    @FindBy (id=SecondPageItemID.emailID)
    public WebElement email;
    
    @FindBy (id=SecondPageItemID.getQuoteBtnID)
    public WebElement carquoteBtn;
    
    
    WebDriver driver;
    
    public SecondPageMethod(WebDriver driver){
    	this.driver=driver;
    	 PageFactory.initElements(driver, this);
    }
    
	// select the car brand
    public void selCarBrand(){
    	new Select (carBrand).selectByVisibleText("BMW");	
    }
    
    // select the manufacture year of car
    public void typCarYear(){
    	carYear.sendKeys("2017");
    }
    
    // type into driver age
    public void typDrvAge(){
    	driverAge.sendKeys("22");
    }
    
    
 // Select the Gender
    public void selGender(){
    	personGender.click();	
    }
    
    // Select the State
    public void selState(){
    	new Select(state).selectByVisibleText("Victoria");   	
    }
    
    // Provide the mail ID
    
    public void typeMailID(){
    	email.sendKeys("abc@123.com");
    }
    
    
    //Click on the Car Quote Btn
    public void clkCarQtBtn(){
    	
    	carquoteBtn.click();
    } 
    
    public void executeAllSecondPageMthd() throws Exception{
    	this.selCarBrand();
    	this.typCarYear();
    	this.typDrvAge();
    	this.selGender();
    	this.selState();
    	this.typeMailID();
    	Thread.sleep(30000);
    	this.clkCarQtBtn();
    	
    }
    
}

