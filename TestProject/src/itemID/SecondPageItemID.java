package itemID;

public interface SecondPageItemID {

	public static final String makeID = "make";
	public static final String yearID= "year";
	public static final String driverAgeID="age";
	public static final String driverGenderID="male";
	public static final String stateID="state";
	public static final String emailID="email";
	public static final String getQuoteBtnID="getquote";
	
	

}
